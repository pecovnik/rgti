var canvas;
var gl;
var shaderProgram;
var skoziVrata1=false;
var skoziVrata2=false;
var prvic=true;
//teksture
var texture1;
var texture2;
var texture3;
var texture4;

//matrike
var mvMatrix = new Float32Array(16);
var pMatrix = new Float32Array(16);
var identityMatrix = new Float32Array(16);
mat4.identity(identityMatrix);


//tabele z informacijami o prostoru
var modelVertices;
var modelIndices;
var modelTexCoords;
var modelPosVertexBufferObject;
var modelTexCoordVertexBufferObject;
var modelIndexBufferObject;

var modelBodiceVertices;
var modelBodiceIndices;
var modelBodiceTexCoords;
var modelBodicePosVertexBufferObject;
var modelBodiceTexCoordVertexBufferObject;
var modelBodiceIndexBufferObject;

var modelStropVertices;
var modelStropIndices;
var modelStropTexCoords;
var modelStropPosVertexBufferObject;
var modelStropTexCoordVertexBufferObject;
var modelStropIndexBufferObject;

var modelVrataVertices;
var modelVrataIndices;
var modelVrataTexCoords;
var modelVrataPosVertexBufferObject;
var modelVrataTexCoordVertexBufferObject;
var modelVrataIndexBufferObject;


//spremenljivke s podatki o igralcu
var pitch = 0;
var pitchRate = 0;
var yaw = 0;
var yawRate = 0;
var xPosition = 0;
var yPosition = -19.5;
var zPosition = 0;
var speed = 0;
var speedUp=0;
var speedDown=0;
var currentlyPressedKeys = {};
var lastTime=0;
var jumping=false;


var count = 1;
var preveriPrviBodici = false;
var preveriDrugiBodici = false;

var newX=0;
var newY=0;

var lastMouseX = 0;
var lastMouseY = 0;


//                 x1            y1          z1          x2            y2          z2 
var skatle = {0:[-20.000063,  -16,       -20.000000,  -15.000063,  -13.774518,  15.000000], //hodnikDolgNegX
			  1:[15.000000,   -16,       -20.000000,  20.000000,   -13.774518,  15.000000], //hodnikDolgPozX
			  2:[-15.000000,  -16,       -20.000000,  15.000000,   -13.772250,  -15.000000],  //hodnikKratek
			  3:[16.000000,   -0.500595, -2.000000,   20.000000,   0.499405,    2.000000],  //podium1
			  4:[11.000000,   -0.500000, -1.000000,   13.000000,   0.500000,    1.000000],   //plate1
			  5:[7.000000,    -0.500000, -1.000000,   9.000000,    0.500000,    1.000000],  //plate2
			  6:[3.000000,    -0.500000, -1.000000,   5.000000,    0.500000,    1.000000],  //plate3
			  7:[-1.000000,   -0.500000, -1.000000,   1.000000,    0.500000,    1.000000],  //plate4
			  8:[-5.000000,   -0.500000, -1.000000,   -3.000000,   0.500000,    1.000000],  //plate5
			  9:[-9.000000,   -0.500000, -1.000000,   -7.000000,   0.500000,    1.000000],  //plate6
			  10:[-13.00000,  -0.500000, -1.000000,   -11.000000,  0.500000,    1.000000],  //plate7
			  11:[-20.000000, -0.498506, -2.000000,   -16.000000,  0.501494,    2.000000]  //podium2
			  }
			  
var bodice = {
	0:[20.000000,   -10.774683,  5.500000, 15.000000, -13.774683,  4.500000], // bodica1.obj, ta prva mimo katere moraš
	1:[20.000000,   -10.774683, -7.500000, 15.000000, -13.774683, -8.500000], // bodica2.obj, ta druga mimo katere moraš
	2:[-15.00000,   -10.774683, -7.500000, -20.00000, -13.774683, -8.500000], // bodica4.obj, tretja mimo katere greš
	3:[-15.00000,   -10.774683,  5.500000, -20.00000, -13.774683,  4.500000]  // bodica3.obj, zadnja bodica, ki te lahko ubije
};








// ta se klice prva
function start() {
	
	loadJSON('./assets/ostalo.json', function(jsonModel){ // cel prostor
		
		loadImage('./assets/tla2.png',function(tex1){
			
			loadJSON('./assets/praveBodice.json', function(jsonBodiceModel) { // sam "bodice"
			
				loadImage('./assets/bodice.png', function(tex2){ 
					
					loadJSON('./assets/vrata.json', function(jsonVrataModel) { // sam vrata
						
						loadImage('./assets/vrata.png', function(tex3) { 
							
							loadJSON('./assets/strop5.json', function(jsonStropModel) { // samo strop
							
								loadImage('./assets/stena.png', function(tex4) {
									// ko dobimo zeljene podatke, klicemo glavno funkcijo
									glavnaFunkcija(jsonModel, jsonBodiceModel, jsonVrataModel, jsonStropModel, tex1, tex2, tex3, tex4);
								});
							});
						});
					});
				});
			});
		});
	});                                                                    
}

var loadJSON = function (url, callback) {
	var request = new XMLHttpRequest();
	request.open("GET", url);
	request.onreadystatechange = function () {
    if (request.readyState == 4) {
      callback(JSON.parse(request.responseText));
    }
  }
  request.send();
};

var loadImage = function (url, callback) {
	var image = new Image();
	image.onload = function () {
		callback(image);
	};
	image.src = url;
};

function glavnaFunkcija(jsonModel, jsonBodiceModel, jsonVrataModel, jsonStropModel, tex1, tex2, tex3, tex4) {
	canvas = document.getElementById("myCanvas");
	
	//inicilaiziramo GL kontekst
	gl = initGL(canvas);     

	if (gl) {
    	gl.clearColor(0.0, 0.0, 0.0, 1.0);                      // Set clear color to black, fully opaque
    	gl.clearDepth(1.0);                                     // Clear everything
    	gl.enable(gl.DEPTH_TEST);                               // Enable depth testing
    	gl.depthFunc(gl.LEQUAL);                                // Near things obscure far things

		//uvozimo in inicializiramo shaderje    	
    	initShaders();
    	//nalozimo teksture in podatke o prostoru v bufferje
    	initBuffers(jsonModel, jsonBodiceModel, jsonVrataModel, jsonStropModel, tex1, tex2, tex3, tex4);     
    	
    	// poskrbimo da se skripta odziva na vporabniski vnos
    	document.onkeydown = handleKeyDown;
    	document.onkeyup = handleKeyUp;
    	canvas.onmousemove = handleMouseMove;
    	//let plm = new PointerLockManager(canvas, handleMouseMove);
    	
    	// zacnemo z risanjem prostora
    	risi();
    	//requestAnimationFrame(risi);
    	
	}
	
}

function initGL(canvas){
	var gl = null;
	try {
    	//Try to grab the standard context. If it fails, fallback to experimental.
    	gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
    	gl.viewportWidth = canvas.width;
    	gl.viewportHeight = canvas.height;
	}catch(e) {}

	//If we don't have a GL context, give up now
	if (!gl) {
    	alert("Unable to initialize WebGL. Your browser may not support it.");
	}
	return gl;
}

function hide(id) {
    var x = document.getElementById(id);
	x.style.display = "none";
}

function unveil(id) {
    var x = document.getElementById(id);
	x.style.display = "block";
	if (id == "smrt") {
		x.style.color = "black";
	} else {
		x.style.color = "green";
	}
}

function initShaders() {
	var fragmentShader = getShader(gl, "shader-fs");
	var vertexShader = getShader(gl, "shader-vs");
  
	// Create the shader program
	shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);
  
	// If creating the shader program failed, alert
	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    	alert("Unable to initialize the shader program.");
	}
  
	// start using shading program for rendering
	gl.useProgram(shaderProgram);
  
	// store location of aVertexPosition variable defined in shader
	shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");

	// turn on vertex position attribute at specified position
	gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

	// store location of aVertexNormal variable defined in shader
	shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");

	// store location of aTextureCoord variable defined in shader
	gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);
  
	// store location of uPMatrix variable defined in shader - projection matrix 
	shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
	// store location of uMVMatrix variable defined in shader - model-view matrix 
	shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
	// store location of uSampler variable defined in shader
	shaderProgram.samplerUniform = gl.getUniformLocation(shaderProgram, "uSampler");
}

function setMatrixUniforms() {
	gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
	gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

function getShader(gl, id) {
	var shaderScript = document.getElementById(id);

	  // Didn't find an element with the specified ID; abort.
	if (!shaderScript) {
    	return null;
	}
  
	// Walk through the source element's children, building the
	// shader source string.
	var shaderSource = "";
	var currentChild = shaderScript.firstChild;
	while (currentChild) {
    	if (currentChild.nodeType == 3) {
        	shaderSource += currentChild.textContent;
    	}
    	currentChild = currentChild.nextSibling;
	}
  
	// Now figure out what type of shader script we have,
	// based on its MIME type.
	var shader;
	if (shaderScript.type == "x-shader/x-fragment") {
    	shader = gl.createShader(gl.FRAGMENT_SHADER);
	}
	else if (shaderScript.type == "x-shader/x-vertex") {
    	shader = gl.createShader(gl.VERTEX_SHADER);
	}
	else {
    	return null;  // Unknown shader type
	}

	// Send the source to the shader object
	gl.shaderSource(shader, shaderSource);

	// Compile the shader program
	gl.compileShader(shader);

	// See if it compiled successfully
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert(gl.getShaderInfoLog(shader));
    return null;
	}

	return shader;
}

function initBuffers(jsonModel, jsonBodiceModel, jsonVrataModel, jsonStropModel, tex1, tex2, tex3, tex4) { 
//function initBuffers(jsonModel, jsonBodiceModel, tex1) {
	
	//tekstura1 - metal.jpg
	texture1 = gl.createTexture();
	texture1.image = tex1;
	
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	// Third texture usus Linear interpolation approximation with nearest Mipmap selection
	gl.bindTexture(gl.TEXTURE_2D, texture1);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture1.image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.generateMipmap(gl.TEXTURE_2D);

	gl.bindTexture(gl.TEXTURE_2D, null);
	
	
	
	//tekstura2
	texture2 = gl.createTexture();
	texture2.image = tex2;
	
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	// Third texture usus Linear interpolation approximation with nearest Mipmap selection
	gl.bindTexture(gl.TEXTURE_2D, texture2);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture2.image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.generateMipmap(gl.TEXTURE_2D);

	gl.bindTexture(gl.TEXTURE_2D, null);



	//tekstura3
	texture3 = gl.createTexture();
	texture3.image = tex3;
	
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	// Third texture usus Linear interpolation approximation with nearest Mipmap selection
	gl.bindTexture(gl.TEXTURE_2D, texture3);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture3.image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.generateMipmap(gl.TEXTURE_2D);

	gl.bindTexture(gl.TEXTURE_2D, null);
	
	
	
	//tekstura4
	texture4 = gl.createTexture();
	texture4.image = tex4;
	
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	// Third texture usus Linear interpolation approximation with nearest Mipmap selection
	gl.bindTexture(gl.TEXTURE_2D, texture4);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture4.image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.generateMipmap(gl.TEXTURE_2D);

	gl.bindTexture(gl.TEXTURE_2D, null);

	
	
	//koordinate - prostor
	modelVertices = jsonModel.meshes[0].vertices;
	modelIndices = [].concat.apply([], jsonModel.meshes[0].faces);
	modelTexCoords = jsonModel.meshes[0].texturecoords[0];
	

	modelPosVertexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, modelPosVertexBufferObject);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(modelVertices), gl.STATIC_DRAW);

	modelTexCoordVertexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, modelTexCoordVertexBufferObject);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(modelTexCoords), gl.STATIC_DRAW);

	modelIndexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, modelIndexBufferObject);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(modelIndices), gl.STATIC_DRAW);
	



	// koordinate - bodice
	modelBodiceVertices = jsonBodiceModel.meshes[0].vertices;
	modelBodiceIndices = [].concat.apply([], jsonBodiceModel.meshes[0].faces);
	modelBodiceTexCoords = jsonBodiceModel.meshes[0].texturecoords[0];
	
	
	modelBodicePosVertexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, modelBodicePosVertexBufferObject);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(modelBodiceVertices), gl.STATIC_DRAW);
	
	modelBodiceIndexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, modelBodiceIndexBufferObject);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(modelBodiceIndices), gl.STATIC_DRAW);

	modelBodiceTexCoordVertexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, modelBodiceTexCoordVertexBufferObject);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(modelBodiceTexCoords), gl.STATIC_DRAW);
	
	
	
	
	// koordinate - vrata
	modelVrataVertices = jsonVrataModel.meshes[0].vertices;
	modelVrataIndices = [].concat.apply([], jsonVrataModel.meshes[0].faces);
	modelVrataTexCoords = jsonVrataModel.meshes[0].texturecoords[0];
	
	
	modelVrataPosVertexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, modelVrataPosVertexBufferObject);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(modelVrataVertices), gl.STATIC_DRAW);
	
	modelVrataIndexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, modelVrataIndexBufferObject);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(modelVrataIndices), gl.STATIC_DRAW);

	modelVrataTexCoordVertexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, modelVrataTexCoordVertexBufferObject);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(modelVrataTexCoords), gl.STATIC_DRAW);
	
	
	
	
	// koordinate - strop
	modelStropVertices = jsonStropModel.meshes[0].vertices;
	modelStropIndices = [].concat.apply([], jsonStropModel.meshes[0].faces);
	modelStropTexCoords = jsonStropModel.meshes[0].texturecoords[0];
	
	
	modelStropPosVertexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, modelStropPosVertexBufferObject);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(modelStropVertices), gl.STATIC_DRAW);
	
	modelStropIndexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, modelStropIndexBufferObject);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(modelStropIndices), gl.STATIC_DRAW);

	modelStropTexCoordVertexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, modelStropTexCoordVertexBufferObject);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(modelStropTexCoords), gl.STATIC_DRAW);

}

function risi(){
	preveriZmago();
	
	//console.log("risem");
	handleMouse();
	
	handleKeys();
	
	animiraj();
	
	// preveri, če smo padli z plat ali pa so nas piknile "špice"
	preveriSmrt();
	
	// zaznavanje trkov
	zaznajKolizijo();

	if (!skoziVrata1 && zPosition >= 19.5 && xPosition > -1 && xPosition < 1 && yPosition < 2.5) {
		skoziVrata1=true;
	}

	if (!skoziVrata2 && xPosition <= -19.5 && zPosition < 13 && zPosition > 11 && yPosition > -14 && yPosition < -11) {
		skoziVrata2=true;
	}
		
	
	if(skoziVrata2==true){
		mat4.rotate(mvMatrix, identityMatrix, degToRad(180), [0, 0, 1]);
		mat4.rotate(mvMatrix, mvMatrix, degToRad(pitch), [1, 0, 0]);	
		mat4.rotate(mvMatrix, mvMatrix, degToRad(yaw), [0, 1, 0]);
	}
	else{
		mat4.rotate(mvMatrix, identityMatrix, degToRad(-pitch), [1, 0, 0]);	
		mat4.rotate(mvMatrix, mvMatrix, degToRad(-yaw), [0, 1, 0]);
	}
	
	
	if (skoziVrata2 && prvic ) {
		xPosition = 18;
		yPosition = 4;
		zPosition = 1;
		prvic=false;
	} 
	
	if (skoziVrata1) {
		xPosition = 18;
		yPosition = -12;
		zPosition = 12;
	} 

	if(skoziVrata2==true){
		mat4.translate(mvMatrix, mvMatrix, [xPosition, yPosition+1, zPosition]);
		
	}
	else{
		mat4.translate(mvMatrix, mvMatrix, [-xPosition, -yPosition-1, -zPosition]);
	}
	
		
	mat4.perspective(pMatrix, glMatrix.toRadian(45), canvas.width / canvas.height, 0.1, 1000.0);
		
	gl.clearColor(0, 0, 0, 1.0);
	gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
		
		
	// risanje celega prostora
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, texture1);
	gl.uniform1i(shaderProgram.samplerUniform, 0);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, modelPosVertexBufferObject);
	gl.vertexAttribPointer(
		shaderProgram.vertexPositionAttribute, // Attribute location
		3, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		3 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		0 // Offset from the beginning of a single vertex to this attribute
	);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, modelTexCoordVertexBufferObject);
	gl.vertexAttribPointer(
		shaderProgram.textureCoordAttribute, // Attribute location
		2, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		2 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		0
	);

	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, modelIndexBufferObject);
	setMatrixUniforms();
	gl.drawElements(gl.TRIANGLES, modelIndices.length, gl.UNSIGNED_SHORT, 0);
	
	


	// risanje vrat 
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, texture3);
	gl.uniform1i(shaderProgram.samplerUniform, 0);

	gl.bindBuffer(gl.ARRAY_BUFFER, modelVrataPosVertexBufferObject);
	gl.vertexAttribPointer(
		shaderProgram.vertexPositionAttribute, // Attribute location
		3, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		3 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		0 // Offset from the beginning of a single vertex to this attribute
	);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, modelVrataTexCoordVertexBufferObject);
	gl.vertexAttribPointer(
		shaderProgram.textureCoordAttribute, // Attribute location
		2, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		2 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		0
	);
	
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, modelVrataIndexBufferObject);
	setMatrixUniforms();
	gl.drawElements(gl.TRIANGLES, modelVrataIndices.length, gl.UNSIGNED_SHORT, 0);





	// risanje stropa 
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, texture4);
	gl.uniform1i(shaderProgram.samplerUniform, 0);

	gl.bindBuffer(gl.ARRAY_BUFFER, modelStropPosVertexBufferObject);
	gl.vertexAttribPointer(
		shaderProgram.vertexPositionAttribute, // Attribute location
		3, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		3 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		0 // Offset from the beginning of a single vertex to this attribute
	);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, modelStropTexCoordVertexBufferObject);
	gl.vertexAttribPointer(
		shaderProgram.textureCoordAttribute, // Attribute location
		2, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		2 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		0
	);
	
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, modelStropIndexBufferObject);
	setMatrixUniforms();
	gl.drawElements(gl.TRIANGLES, modelStropIndices.length, gl.UNSIGNED_SHORT, 0);




	// risanje bodic
	premakniBodice();

	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, texture2);
	gl.uniform1i(shaderProgram.samplerUniform, 0);

	gl.bindBuffer(gl.ARRAY_BUFFER, modelBodicePosVertexBufferObject);
	gl.vertexAttribPointer(
		shaderProgram.vertexPositionAttribute, // Attribute location
		3, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		3 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		0 // Offset from the beginning of a single vertex to this attribute
	);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, modelBodiceTexCoordVertexBufferObject);
	gl.vertexAttribPointer(
		shaderProgram.textureCoordAttribute, // Attribute location
		2, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		2 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		0
	);
	
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, modelBodiceIndexBufferObject);
	setMatrixUniforms();
	gl.drawElements(gl.TRIANGLES, modelBodiceIndices.length, gl.UNSIGNED_SHORT, 0);



	skoziVrata1=false;
	
	
	// vsakih 60 countov se bodo preverjale bodice na drugi strani, enkrat na levi enkrat na desni
	count++;
	if (count > 60){
		count = 1;
	}
	requestAnimationFrame(risi);
	
}

function zaznajKolizijo(){
	if(yPosition<-19.5 ){
		var odstopanjeY=-19.5-yPosition;
		yPosition+=odstopanjeY;
		speedDown=0;
		if(jumping==true){
			speedUp=0;
			jumping=false;
		}
	}
	else if(yPosition>19.5){
		var odstopanjeY2=yPosition-19.5;
		yPosition-=odstopanjeY2;
	}

	if(xPosition<-19.5 ){
		var odstopanjeX1=-19.5-xPosition;
		xPosition+=odstopanjeX1;
	}
	else if(xPosition>19.5){
		var odstopanjeX2=xPosition-19.5;
		xPosition-=odstopanjeX2;
	}
	
	if(zPosition<-19.5 ){
		var odstopanjeZ1=-19.5-zPosition;
		zPosition+=odstopanjeZ1;
	}
	else if(zPosition>19.5){
		var odstopanjeZ2=zPosition-19.5;
		zPosition-=odstopanjeZ2;
	}	
	
	//preveri kolizijo z vsemi skatlami in po potrebi popravi	
	for(i in skatle){
		zaznajInPopraviSkatle(i);
	}
	
}

function zaznajInPopraviSkatle(i){
	if(xPosition>skatle[i][0]&&xPosition<skatle[i][3]&&yPosition>skatle[i][1]&&yPosition<skatle[i][4]&&zPosition>skatle[i][2]&&zPosition<skatle[i][5] ){
	
	
	// kolizija po y osi	
		var razl2Y=Math.abs(skatle[i][4]-yPosition);
		var razl1Y=Math.abs(skatle[i][1]-yPosition);
		if(razl2Y<razl1Y){
			yPosition+=razl2Y;
			speedDown=0;
			if(jumping==true){
				speedUp=0;
				jumping=false;
			}
		}
		else{
			yPosition-=razl1Y;
		}
	/*
	// kolizija po x osi
	var razl2X=Math.abs(skatle[i][3]-xPosition);
	var razl1X=Math.abs(skatle[i][0]-xPosition);
	if(razl2X<razl1X){
		xPosition+=razl2X;
	}
	else{
		xPosition-=razl1X;
	}
	
	// kolizija po z osi
	var razl2Z=Math.abs(skatle[i][5]-zPosition);
	var razl1Z=Math.abs(skatle[i][2]-zPosition);
	if(razl2Z<razl1Z){
		zPosition-=razl2Z;
	}
	else{
		zPosition+=razl1Z;
	}		
		
	*/	
		

	}
}

function animiraj(){
	var timeNow = new Date().getTime();
	
	if(lastTime!=0){
		var elapsed = timeNow - lastTime;
		
		var tempSpeed=speedUp-speedDown;
		
		yPosition += tempSpeed*elapsed;
		speedDown=speedDown+0.001;
		
		
		if(speed != 0 ){
			if(skoziVrata2==true){
				xPosition += Math.sin(degToRad(-yaw)) * speed * elapsed;
    			zPosition += Math.cos(degToRad(-yaw)) * speed * elapsed;	
			}
			else{
				xPosition -= Math.sin(degToRad(yaw)) * speed * elapsed;
    			zPosition -= Math.cos(degToRad(yaw)) * speed * elapsed;	
			}
			

		}
			
    	yaw += yawRate*elapsed;
    	pitch += pitchRate*elapsed;
    	
		
		if(pitch<-90){
			pitch=-90;
		}
		else if(pitch>90){
			pitch=90;
		}		
	}
	
	lastTime = timeNow;
}

function handleKeys() {
	
	if (currentlyPressedKeys[65]) {
    	// Left cursor key or A
    	yawRate = 0.1;
	}
	else if (currentlyPressedKeys[68]) {
    	// Right cursor key or D
    	yawRate = -0.1;
	}
	

	if (currentlyPressedKeys[87]) {
    	// Up cursor key or W
    	speed = 0.01;
	}
	else if (currentlyPressedKeys[83]) {
    	// Down cursor key
    	speed = -0.01;
	}
	else {
    	speed = 0;
	}
  
	if (currentlyPressedKeys[32]) {
    	// spacebar
    	if(jumping==false){
    		jumping=true;
    		speedUp = 0.02;
		}
	}
}

function handleKeyDown(event){
	// storing the pressed state for individual key
	currentlyPressedKeys[event.keyCode] = true;
}

function handleKeyUp(event){
	// reseting the pressed state for individual key
	currentlyPressedKeys[event.keyCode] = false;
}

function degToRad(degrees){
	return degrees * Math.PI / 180;
}

// handleMouse
//
// Called every time before redeawing the screen for keyboard
// input handling. Function continuisly updates helper variables.
//
function handleMouseMove(event) {

  //console.log("miska klik");
  newX = event.clientX;
  newY = event.clientY;

}

function handleMouse(){ 

	yawRate = -(newX - lastMouseX)/25; 
	pitchRate = -(newY- lastMouseY)/25;
	
	lastMouseX = newX;
	lastMouseY = newY;
	
}

function preveriSmrt() {
	if (skoziVrata2 && yPosition < -18) {
		hide('myCanvas');
		unveil('smrt');
	}
	
	if (count <= 30) {
		preveriPrviBodici = true;
		preveriDrugiBodici = false;
	}
	if (count > 30) {
		preveriPrviBodici = false;
		preveriDrugiBodici = true;
	}
	
	if (preveriBodice()) {
		hide('myCanvas');
		unveil('smrt');
	}
}

function preveriZmago() {
	if (skoziVrata2 && yPosition < 3 && yPosition > -3 && xPosition < -19.2 && zPosition < 1 && zPosition > -1) {
		hide('myCanvas');
		unveil('zmaga');
	}
}

// vse "bodice" se morajo po x-osi prestavit za +5 vsakih 60 iteracij -> TODO
function premakniBodice() {
	
	if (count <= 30) {
		mat4.translate(mvMatrix, mvMatrix, [5, 0, 0]);
	}
	if (count > 30) {
		mat4.translate(mvMatrix, mvMatrix, [0, 0, 0]);
	}
	
}

function preveriBodice() {
	if (preveriPrviBodici) {
		for (var i = 0; i < 2; i++) {
			if (xPosition < bodice[i][0] && xPosition > bodice[i][3] && yPosition < bodice[i][1] && yPosition > (bodice[i][4] - 0.5) && zPosition < bodice[i][2] && zPosition > bodice[i][5]) {
				return true;
			}
		}
	}
	if (preveriDrugiBodici) {
		for (var i = 2; i < 4; i++) {
			if (xPosition < bodice[i][0] && xPosition > bodice[i][3] && yPosition < bodice[i][1] && yPosition > (bodice[i][4] - 0.5) && zPosition < bodice[i][2] && zPosition > bodice[i][5]) {
				return true;
			}
		}
	}
	return false;
}

/*
class PointerLockManager {

    constructor(elem, moveCallback) {
      this.elem = elem || document.body;
      this.moveCallback = moveCallback || function(e) {};
      this.elem.addEventListener('click', this.lock.bind(this));
  
      document.addEventListener('pointerlockchange', this.onChangeHandler.bind(this));
    }
  
    isLocked() {
      return document.pointerLockElement === this.elem;
    }
  
    lock() {
      if(!this.isLocked()){
        this.elem.requestPointerLock();
        document.addEventListener("mousemove", this.moveCallback, false);
      }
    }
  
    unlock() {
      if(this.isLocked()){
        document.removeEventListener("mousemove", this.moveCallback, false);
        document.exitPointerLock();
      }
    }
  
    onChangeHandler() {
    	console.log('pointer is '+(this.isLocked() ? 'locked' : 'unlocked'));
    }
}*/